/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    DBG ("width: " << getWidth() << " height: " << getHeight());
    
    textButton1.setButtonText ("Click Me");
    addAndMakeVisible (textButton1);
    textButton1.addListener (this);
    textButton1.setBounds (10, 10, getWidth() - 20, 40);
    
    slider1.setSliderStyle (Slider::LinearHorizontal);
    addAndMakeVisible (slider1);
    slider1.setBounds (10, 50, getWidth() - 20, 40);
    
}

void MainComponent::buttonClicked (Button* button)
{
    DBG ("Button Clicked\n");
    
}





